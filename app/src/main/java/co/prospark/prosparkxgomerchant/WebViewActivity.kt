package co.prospark.prosparkxgomerchant


import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import android.webkit.WebChromeClient
import android.util.Log
import android.view.View
import android.view.Window
import android.webkit.PermissionRequest
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_web_view.*




class WebViewActivity : AppCompatActivity() {
    lateinit var myWebView: WebView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        getWindow().requestFeature(Window.FEATURE_PROGRESS)
        setContentView(R.layout.activity_web_view)
        // Makes Progress bar Visible
        getWindow().setFeatureInt( Window.FEATURE_PROGRESS, Window.PROGRESS_VISIBILITY_ON);

        getSupportActionBar()?.setHomeAsUpIndicator(R.drawable.ic_close_24)

        val url = intent.getStringExtra("url")
        myWebView = findViewById(R.id.webview_layout)
        myWebView.webChromeClient = object : WebChromeClient() {
            var customView: View? = null
            var callback: CustomViewCallback? = null
            var originalOrientation: Int = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
            var originalVisibility: Int = View.INVISIBLE
            override fun onPermissionRequest(request: PermissionRequest?) {
                request?.grant(request.resources);
            }

            override fun onProgressChanged(view: WebView, progress: Int) {
                Log.d("Progress", progress.toString())
                loadingWeb.progress = progress
                if (progress == 100) {
                    setTitle(R.string.app_name)
                    loadingWeb.visibility = View.GONE
                } else {
                    setTitle("Loading...");
                    loadingWeb.visibility = View.VISIBLE
                }
            }

            override fun onShowCustomView(view: View?, callback: CustomViewCallback?) {
                if (customView != null) {
                    onHideCustomView()
                    return
                }
                customView = view
                originalVisibility = window.decorView.systemUiVisibility
                originalOrientation = requestedOrientation
                (window.decorView as FrameLayout).addView(this.customView, FrameLayout.LayoutParams(-1, -1))
                window.decorView.systemUiVisibility = 3846 or View.SYSTEM_UI_FLAG_LAYOUT_STABLE

            }

            override fun onHideCustomView() {
                (window.decorView as FrameLayout).removeView(customView)
                customView = null
                window.decorView.systemUiVisibility = originalVisibility
                requestedOrientation = originalOrientation
                callback?.onCustomViewHidden()
                callback = null
            }
        }

        myWebView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {

                if(url!!.contains("https://gojekmerchant-stg.prospark.co/close?gobiz_action=close")) {
                    finish()
                } else {
                    if (savedInstanceState == null) {
                        view?.loadUrl(url)
                    }
                }

                return true
            }
        }
        myWebView.settings.javaScriptEnabled = true
        myWebView.settings.domStorageEnabled= true
        myWebView.settings.loadWithOverviewMode= true
        myWebView.settings.useWideViewPort= true
        myWebView.settings.displayZoomControls= false
        myWebView.settings.setSupportZoom(true)
        myWebView.isVerticalScrollBarEnabled = true
        myWebView.settings.defaultTextEncodingName = "utf-8"
        if (savedInstanceState == null) {
            myWebView.loadUrl(url)
        }

    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        myWebView.saveState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        myWebView.restoreState(savedInstanceState)
    }
    // this function can ovveride back navigation on action bar
    /*override fun onSupportNavigateUp(): Boolean {
       if (myWebView.canGoBack()) {
            myWebView.goBack()
        } else {
            super.onBackPressed()
        }
        return true
    }*/
    override fun onBackPressed() {
        if (myWebView.canGoBack()) {
            myWebView.goBack()
        } else {
            super.onBackPressed()
        }
    }
}
